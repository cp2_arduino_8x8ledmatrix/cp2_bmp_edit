/*
 * Auteur 1: Jethro Pans
 * Auteur 2: Oghuzan Guney
 * https://gitlab.com/cp2_arduino_8x8ledmatrix/cp2_bmp_edit/blob/master/bmp_edit.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char AFBEELDING1[60] = "C:\\Users\\11700398\\Documents\\"; //De invoer van de afbeeldingen gebeurt softcoded. Daarom staat er enkel de map waar de afbeelding in zit.
char AFBEELDING2[60] = "C:\\Users\\11700398\\Documents\\";
char img1[20];
char img2[20];
long int hoogte;
long int breedte;
long int grootte;

typedef struct RGB {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} RGB; //Hier zijn de 3 variabelen van de kleuren aangemaakt. Later in de code vinden we ze terug om de pixels aan te geven.

int main()
{
    printf("Plaats uw afbeelding (BMP-bestand) eerst in de Documents-folder.\n");           //Hier wordt gevraagd naar de naam van de afbeelding en de naam voor de nieuwe afbeelding.
    printf("Geef de naam van uw te inverteren afbeelding in: \n");
    scanf("%s", img1);// de naam van een array is het address van het eerst element
    printf("Geef hier in hoe de nieuwe afbeelding zal noemen:\n");
    scanf("%s", img2);
    printf("Hoogte van de afbeelding:\n");
    scanf("%d", &hoogte);
    printf("Breedte van de afbeelding:\n");
    scanf("%d", &breedte);

        strcat(AFBEELDING1, img1);          //Hier worden de namen van de afbeeldingen in het path gezet, zodat ze gevonden kunnen worden door het programma.
        strcat(AFBEELDING2, img2);

    printf("%s \n", AFBEELDING1);
    printf("%s \n", AFBEELDING2);

    grootte = hoogte * breedte;
    grootte *= 3;
    grootte += 54;

    FILE * bmp = fopen (AFBEELDING1, "rb");
    FILE * bmp2 = fopen (AFBEELDING2, "wb");//Zonet heb ik 2 file pointers gemaakt. 1 wijst naar de originele afbeelding en de andere naar de nieuw aangemaakte afbeelding.
    int getal[54];
    int i;
    RGB colors;
    if(bmp == NULL){
        printf("Er is geen afbeelding gevonden. \n");
        exit(EXIT_FAILURE);
    }//Als het programma de originele bmp niet kan vinden zal deze een melding geven en afsluiten. Dus het is van belang vanboven het juiste adres in te geven.
    for(i=0; i<54; i++){
    fread(&getal[i], 1, 1, bmp);
    printf("%x\n", getal[i]);
    fwrite(&getal[i], 1, 1, bmp2);
    }//Net werd eerst de header ingelezen. Dit is om te bekijken welke waardes de afbeelding heeft van bijvoorbeeld de breedte of de hoogte. Nadien worden deze uitgeprint, zodat de gebruiker kan zien met welke afbeelding hij of zij te maken heeft.
    for (i=54; i<=grootte; i++) // 3*(B*H)+54. Dit is de formule die werd gebruikt om de grootte te bepalen.
    {
            fread(&colors, sizeof(RGB), 1, bmp);

            colors.b = ~colors.b;
            colors.g = ~colors.g;
            colors.r = ~colors.r;

            fwrite(&colors, sizeof(RGB), 1, bmp2);
}//De voorbije lus heeft de pixels een voor een apart genomen, geïnverteerd en dan geplaatst in de nieuwe afbeelding. Dat golfje geeft de invertie aan.

    fclose(bmp);
    fclose(bmp2);//Als laatste worden de file pointers gesloten.
    return 0;
}
